﻿using System;
using System.Diagnostics;

namespace Kontr2
{
    internal class Program
    {
        static int[] arr;
        static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Stopwatch sw = new Stopwatch();
            Console.Title = "Quicksort";
            Console.Write("Size of array:");
            int size;
            while(!int.TryParse(Console.ReadLine(), out size) || size<=0)
            {
                Console.Write("Error!Size of array:");
            }
            Random random = new Random();
            arr = new int[size];
            //Console.WriteLine("Unsorted array:");
            for (int i = 0; i < size; i++)
            {
                arr[i] = random.Next(-50,51);
                //Console.Write($"{arr[i]}   ");
            }
            Console.WriteLine();
            sw.Start();
            QuickSort(0,size - 1);
            sw.Stop();
            //Console.WriteLine("Sorted array:");
            //for (int i = 0; i < size; i++)
            //{
            //    Console.Write($"{arr[i]}   ");
            //}
            //Console.WriteLine();
            Console.WriteLine($"Time of sort:{sw.Elapsed.TotalMilliseconds} ml");
        }
        static void QuickSort(int rightlim,int leftlim)
        {
                int rightindex = rightlim, leftindex = leftlim, pivot = rightlim;
                while (rightindex <= leftindex)
                {
                    while (arr[rightindex] < arr[pivot])
                    {
                        rightindex++;
                    }
                    while (arr[leftindex] > arr[pivot])
                    {
                        leftindex--;
                    }
                    if (rightindex <= leftindex)
                    {
                        int temp = arr[rightindex];
                        arr[rightindex] = arr[leftindex];
                        arr[leftindex] = temp;
                        rightindex++;
                        leftindex--;
                    }
                }
                if (rightlim < leftindex)
                    QuickSort(rightlim, leftindex);
                if (rightindex < leftlim)
                    QuickSort(rightindex, leftlim);
        }
    }
}
